steps = [
    [
        ## Create table
        """
        CREATE TABLE vacations (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL,
            from_date DATE NOT NULL,
            thoughts TEXT
        );
        """,
        ## Drop table
       """
        DROP TABLE vacations;
       """
    ]
]