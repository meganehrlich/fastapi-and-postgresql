# -- FAST API endpoints
from typing import Union, List
from fastapi import APIRouter, Depends, Response
from queries.vacations import VacationIn, VacationOut, VacationRepository, Error

router = APIRouter()

@router.post("/vacations", response_model=Union[VacationOut, Error])
def create_vacation(
    vacation: VacationIn, 
    response: Response,
    repo: VacationRepository = Depends()
):
    response.status_code = 400
    return repo.create(vacation)
 
@router.get("/vacations", response_model=Union[list[VacationOut], Error])
def get_all(
    repo: VacationRepository = Depends(),

):
    return repo.get_all()