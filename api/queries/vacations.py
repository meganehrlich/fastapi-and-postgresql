from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool



class VacationIn(BaseModel):
    name: str
    from_date: str
    thoughts: Optional[str]

class VacationOut(BaseModel):
    id: int
    name: str
    from_date: str
    thoughts: Optional[str]

class Error(BaseModel):
    message: str


class VacationRepository:
    def get_all(self) -> Union[Error, List[VacationOut]]:
        try:
            with pool.connection() as conn:
                # Get a cursor
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name, from_date, thoughts
                        FROM vacations
                        ORDER BY from_date
                        """
                    )
                    result=[]
                    for record in db:
                        vacation = VacationOut(
                            id = record[0],
                            name = record[1],
                            from_date= record[2],
                            thoughts= record[3],
                        )
                        result.append(vacation)
                    return result

        except Exception as e:
            return {"message": "could not get all vacations"}


    def create(self, vacation: VacationIn) -> VacationOut:
        # Connect to database
        with pool.connection() as conn:
            # Get a cursor
            with conn.cursor() as db:
                # Run insert Statement
                result = db.execute(
                    """
                    INSERT INTO vacations
                        (name, from_date, thoughts)
                    VALUES
                        (%s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        vacation.name, 
                        vacation.from_date, 
                        vacation.thoughts
                    ]

                )
                id = result.fetchone()[0]
                # Return new data
                old_data = vacation.dict()
                return {"message": "error"}
